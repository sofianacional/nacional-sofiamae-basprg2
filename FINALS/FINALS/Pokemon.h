#pragma once
#include <iostream>
#include <string>

using namespace std;

class Pokemons
{
public:
	string name;
	int baseHp = 0;
	int hp = 0;
	int lvl = 0;
	int baseDam = 0;
	int exp = 0;
	int expToNxtLvl = 0;
	int count = 0;

	Pokemons(string name, int hp, int lvl, int baseDam, int baseHp, int exp, int expToNxtLvl);
	void starter();
	void generateWildPokemon();
	void displayStats();
	void pokemonEncounter(Pokemons* pokemon);
	void applyDamage(Pokemons* pokemon);
	void pokemonCenter();
	void leveling(Pokemons* myPokemon);
	void encounterPokemon(Pokemons*& encounter, int random);
};
