#include <iostream>
#include <string>
#include <conio.h>
#include "Player.h"
#include "Pokemon.h"

using namespace std;

Pokemons::Pokemons(string name, int hp, int lvl, int baseDam, int baseHp, int exp, int expToNxtLvl)
{
	this->name = name;
	this->hp = hp;
	this->baseHp = baseHp;
	this->lvl = lvl;
	this->baseDam = baseDam;
	this->exp = exp;
	this->expToNxtLvl = expToNxtLvl;
}

void Pokemons::starter()
{
	this->baseHp = (this->baseHp * 0.25) * this->lvl;
	this->hp = this->baseHp;
	this->baseDam = (this->baseDam / 9) * this->lvl;
	this->expToNxtLvl = (this->baseHp * 0.25) * this->lvl;
}

void Pokemons::generateWildPokemon()
{
	this->name = name;
	this->lvl = rand() % 10 + 1;
	this->baseHp = (this->baseHp * 0.25) * this->lvl;
	this->hp = this->baseHp;
	this->baseDam = (this->baseDam / 9) * this->lvl;
	this->expToNxtLvl = (this->baseHp * 0.25) * this->lvl;
}

void Pokemons::displayStats()
{
	cout << endl;
	cout << name << " Stats:\n";
	cout << "Level: " << this->lvl << endl;
	cout << "Health Points: " << this->hp << "/" << this->baseHp << endl;
	cout << "Damage: " << this->baseDam << endl;
	cout << "Exp: " << this->exp << "/" << this->expToNxtLvl << endl << endl;
}

void Pokemons::pokemonEncounter(Pokemons* pokemon)
{
	char choice;
	cout << "You have encountered a " << pokemon->name << endl;
	pokemon->displayStats();

	while (pokemon->hp > 0 && this->hp > 0)
	{
		cout << "What would you like to do?\n[a] Fight       [b] Catch      [c] Run Away\n";
		cin >> choice;

		if (choice == 'a' || choice == 'A')
		{
			this->applyDamage(pokemon);
			if (pokemon->hp >= 0)
				pokemon->applyDamage(this);
		}
		else if (choice == 'b' || choice == 'B') // catch
		{
			int catchChance = rand() % 10 + 1;
			cout << ".\n" << _getch << ". .\n" << _getch << ". . .\n";
			system("pause");

			if (catchChance <= 3)
			{
				/*Pokemons* newPokemon = new Pokemons();
				player->myPokemon[player->pokemonCount] = newPokemon;
				player->pokemonCount++;*/
			}
			else
			{
				cout << "You failed to catch " << pokemon->name << endl;
			}

		}
		else if (choice == 'c' || choice == 'C') // run away
		{
			cout << "You ran away.\n";
			break;
		}
		system("pause");
	}

	if (pokemon->hp <= 0) //Win
	{
		cout << this->name << " won.\n";
		if (this->exp >= this->expToNxtLvl)
		{
			leveling(this);
		}
	}
}

void Pokemons::applyDamage(Pokemons* pokemon)
{
	int chance = rand() % 10 + 1;
	cout << this->name << " attacked " << pokemon->name << endl;
	if (chance >= 3)
	{
		pokemon->hp -= this->baseDam;
		cout << pokemon->name << " took " << this->baseDam << " damage.\n";
		cout << pokemon->name << " has " << pokemon->hp << " hp left.\n";
		cout << endl;
	}
	else
		cout << pokemon->name << "'s attack missed.\n";
}
	
void Pokemons::pokemonCenter()
{
		this->hp = this->baseHp;
}

void Pokemons::leveling(Pokemons * myPokemon)
{
	myPokemon->baseHp += myPokemon->baseHp * 0.15;
	myPokemon->baseDam += myPokemon->baseDam * 0.10;
	myPokemon->expToNxtLvl += myPokemon->expToNxtLvl * 0.20;
	myPokemon->exp = 0;
}

void Pokemons::encounterPokemon(Pokemons*& encounter, int random) {

	int lvl = rand() % 10 + 1;

	Pokemons* pokemon[15];
	pokemon[0] = new Pokemons("Bulbusaur", 45, lvl, 49, 45, 0, 64);
	pokemon[1] = new Pokemons("Charmander", 39, lvl, 52, 39, 0, 62);
	pokemon[2] = new Pokemons("Squirtle", 44, lvl, 48, 44, 0, 63);
	pokemon[3] = new Pokemons("Caterpie", 45, lvl, 30, 45, 0, 39);
	pokemon[4] = new Pokemons("Weedle", 40, lvl, 35, 40, 0, 39);
	pokemon[5] = new Pokemons("Pidgey", 40, lvl, 45, 40, 0, 50);
	pokemon[6] = new Pokemons("Rattata", 30, lvl, 56, 30, 0, 51);
	pokemon[7] = new Pokemons("Spearow", 40, lvl, 60, 40, 0, 52);
	pokemon[8] = new Pokemons("Ekans", 35, lvl, 60, 35, 0, 58);
	pokemon[9] = new Pokemons("Pikachu", 35, lvl, 55, 35, 0, 112);
	pokemon[10] = new Pokemons("Sandshrew", 50, lvl, 75, 50, 0, 60);
	pokemon[11] = new Pokemons("Nidoran", 55, lvl, 47, 55, 0, 55);
	pokemon[12] = new Pokemons("Clefairy", 70, lvl, 45, 70, 0, 113);
	pokemon[13] = new Pokemons("Vulpix", 38, lvl, 41, 38, 0, 60);
	pokemon[14] = new Pokemons("JIgglypuff", 115, lvl, 45, 115, 0, 95);

	for (int i = 1; i < pokemon[random]->lvl; i++) {
		pokemon[random]->baseHp += (pokemon[random]->baseHp * 0.15);
		pokemon[random]->baseDam += (pokemon[random]->baseDam * 0.10);
		pokemon[random]->expToNxtLvl += (pokemon[random]->expToNxtLvl * 0.20);
	}

	pokemon[random]->hp = pokemon[random]->baseHp;

	encounter = pokemon[random];
}



