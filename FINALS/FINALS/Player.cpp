#include <iostream>
#include <string>
#include <time.h>
#include "Player.h"

using namespace std;

Player::Player(string name, int x, int y)
{
	this->x = x;
	this->y = y;
	this->name = name;
}

void Player::location()
{
	string location;
	char move;

	cout << "[w] Up      [a] Left\n[s] Down    [d] Right\n";
	cin >> move;

	if (move == 'w' || move == 'W')
		this->y++;
	else if (move == 'a' || move == 'A')
		this->x--;
	else if (move == 's' || move == 'S')
		this->y--;
	else if (move == 'd' || move == 'D')
		this->x++;

	if (x >= -2 && x <= 2 && y >= -2 && y <= 2)
		location = "Pallet Town";
	else if (x >= -9 && x <= 7 && y >= 3 && y <= 8)
		location = "Route 1";
	else
		location = "Unknown Location";

	cout << "Your location is (" << x << ", " << y << ")\nYou are in " << location << endl;

}

