#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include "Player.h"
#include "Pokemon.h"

using namespace std;

int main()
{
	srand(time(NULL));

	string playerName;
	int x = 0, y = 0;

	Player* player = new Player(playerName, x, y);
	Pokemons* myPokemon[6];

	// starter
	char starter;
	cout << "Hello Trainer! Enter your name: ";
	cin >> playerName;
	system("cls");

	cout << "Choose one starter pokemon:\n\n [a] Bulbasaur      Grass Type    Level 5\n [b] Charmander     Fire Type     Level 5\n [c] Squirtle       Water Type    Level 5\n\n Choice: ";
	cin >> starter;

	if (starter == 'a' || starter == 'A')
	{
		Pokemons* newPokemon = new Pokemons("Bulbusaur", 59, 5, 59, 19, 0, 39);
		player->pokemonCount = 0;
		player->myPokemon[player->pokemonCount] = newPokemon;
	}
	else if (starter == 'b' || starter == 'B')
	{
		Pokemons* newPokemon = new Pokemons("Charmander", 59, 5, 59, 19, 0, 39);
		player->pokemonCount = 0;
		player->myPokemon[player->pokemonCount] = newPokemon;
	}
	else if (starter == 'c' || starter == 'C')
	{
		Pokemons* newPokemon = new Pokemons("Squirtle", 59, 5, 59, 19, 0, 39);
		player->pokemonCount = 0;
		player->myPokemon[player->pokemonCount] = newPokemon;
	}
	cout << "You chose " << player->myPokemon[0]->name << " !" << endl;
	player->pokemonCount++;
	system("pause");
	system("cls");

	// Main Action
	char action;
	do
	{
		cout << "[a] Move      [b] Pokemon";
		if (player->x >= -2 && player->x <= 2 && player->y >= -2 && player->y <= 2)
			cout << "      [c] Pokemon Center";
		cout << endl;
		cin >> action;
		system("cls");

		if (action == 'a' || action == 'A')
		{
			player->location();
			int encounter = rand() % 10 + 1;
			if (player->x >= -9 && player->x <= 7 && player->y >= 3 && player->y <= 8)
			{
				if (encounter <= 3)
				{
					system("cls");
					Pokemons* newPokemon;
					int randomPokemon = rand() % 15;
					newPokemon->encounterPokemon(newPokemon, randomPokemon);

					player->myPokemon[0]->pokemonEncounter(newPokemon);

					if (player->myPokemon[0]->exp >= player->myPokemon[0]->expToNxtLvl) // level up
						player->myPokemon[0]->leveling(player->myPokemon[0]);
				}
			}
		}
		else if (action == 'b' || action == 'B')
			player->myPokemon[0]->displayStats();
		
		if (player->x >= -2 && player->x <= 2 && player->y >= -2 && player->y <= 2)
		{
			if (action == 'c' || action == 'C')
			player->myPokemon[0]->pokemonCenter();
		}
		system("pause");
		system("cls");
	} while (true);

	system("pause");
	return 0;
}